from pypodio2 import api
import tkinter as tk
import tkinter.simpledialog as simpDia
import tkinter.filedialog as fileDia
import pandas as pd

candidateID = 25009053
founderId = 24996379
country = "Jordan"
data = {"grant_type":"password",
    "client_id":"extractqualifyingemails",
    "redirect_uri":"andrewwilliamcurran.com",
    "client_secret":"qkqCKhfLQdqkypYF6ZtoVyAayvgvlKgo7a41VmIHUIidriV6Gsk0EhlTFuOvDASy"
    }

def enterUsername():
    root = tk.Tk()
    root.withdraw()
    x = simpDia.askstring("Enter your Username","Username:",parent=root)
    root.destroy()
    return x
    
def getpwd():
    root = tk.Tk()
    root.withdraw()
    x = simpDia.askstring("Enter your Password","Password:",parent=root,show="*")
    root.destroy()
    return x

def badUserPass():
    def close_window(): 
        root.destroy()
    root = tk.Tk()
    
    root.title("Bad Username Password Combination!") 
    root.geometry("400x50")
    button = tk.Button(root, text = "OK", command = close_window)
    button.pack()

    root.mainloop()
    
    return

def getCountry():
    root = tk.Tk()
    root.withdraw()
    x = simpDia.askstring("Enter the country","Country:",parent=root)
    root.destroy()
    return x

def chooseSaveFile():
    root = tk.Tk()
    root.withdraw()
    filename=fileDia.asksaveasfilename(title="Select where to save the output Excel",defaultextension=".xlsx")
    root.destroy()
    return filename

if __name__ == '__main__':
    data['username'] = enterUsername()
    data['password'] = getpwd()
    try:
        c = api.OAuthClient(
            data['client_id'],
            data['client_secret'],
            data['username'],
            data['password']
            )
    except Exception:
        badUserPass()
        quit()
        
    country = getCountry()
    payLoad = {"filters":
        {
            "p1qual":{"from":1,"to":1},
            "country":country,
        },
        'limit':500}
    
    resp = c.Item.filter(founderId,attributes=payLoad)
    listDict = {"Business":[],"Name":[],"Position":[],"Email":[],"Country":[]}
    for item in resp['items']:
        for field in item['fields']:
            if field['label']=="Business":
                listDict["Business"].append(field['values'][0]["value"]["title"])
            if field['label']=="Name":
                listDict["Name"].append(field['values'][0]["value"])
            if field['label']=="Position":
                listDict["Position"].append(field['values'][0]['value']['text'])
            if field['label']=="Email":
                listDict["Email"].append(field['values'][0]["value"])
            if field['label']=="country":
                listDict["Country"].append(field['values'][0]["value"])
    saveFile = chooseSaveFile()
    pd.DataFrame(listDict).to_excel(saveFile,index=False)
        
